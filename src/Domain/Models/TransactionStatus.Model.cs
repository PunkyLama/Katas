﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public enum TransactionStatus
    {
        Approuved = 0,
        Rejected = 1
    }
}
